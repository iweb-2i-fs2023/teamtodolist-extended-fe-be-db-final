import express from 'express';
import { router as todoRouter } from './backend/todo/todo.routes.js';
import { router as memberRouter } from './backend/member/member.routes.js';
import mongoose from 'mongoose';

const app = express();

mongoose.connect('mongodb://localhost/teamtodolist');

app.use(express.static('frontend'));

app.use(express.json());
app.use('/api/todos', todoRouter);
app.use('/api/members', memberRouter);

mongoose.connection.once('open', () => {
  console.log('Connected to MongoDB');
  app.listen(3001, () => {
    console.log('Server listens to http://localhost:3001');
  });
});
