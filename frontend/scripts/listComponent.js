const LIST_EVENT = {
  UPDATE: 'Update',
  DELETE: 'Delete',
};

const ID_TODO_LIST = 'todo-list';
const ID_MEMBER_LIST = 'member-list';

function createTodoList(todos, members, listEventCallback) {
  function createTodoItem(todo) {
    // Listen Item erstellen
    const elLi = document.createElement('li');
    // Checkbox erstellen
    const elCheck = document.createElement('input');
    elCheck.setAttribute('type', 'checkbox');
    const todoId = 'check-' + todo._id;
    elCheck.setAttribute('id', todoId);
    // Event-Listener für Checkbox hinzufügen
    elCheck.addEventListener('change', () => {
      // Status erledigt / nicht erledigt in Backend updaten
      listEventCallback(LIST_EVENT.UPDATE, todo._id, {
        description: todo.description,
        responsible: todo.responsible,
        isDone: elCheck.checked,
      });
    });
    // Checkbox Item hinzufügen
    elLi.appendChild(elCheck);
    // Label erstellen
    const elLabel = document.createElement('label');
    elLabel.setAttribute('for', todoId);
    // Todo als Inhalt Label hinzufügen
    elLabel.textContent = todo.description;
    // Label Item hinzufügen
    elLi.appendChild(elLabel);
    // Verantwortliche Person hinzufügen
    const elResp = document.createElement('div');
    elResp.classList.add('responsible');
    const resp = members.find((member) => member._id === todo.responsible);
    elResp.textContent = resp.name;
    elLi.appendChild(elResp);
    if (todo.isDone) {
      // Styling für erledigte Todos richtig setzen
      elCheck.checked = true;
      elLabel.classList.add('done');
      // Delete Button
      addDeleteButton(elLi, () =>
        listEventCallback(LIST_EVENT.DELETE, todo._id)
      );
    }
    return elLi;
  }

  const compareTodos = (todo1, todo2) => {
    if (todo1.responsible < todo2.responsible) {
      return -1;
    }
    if (todo1.responsible > todo2.responsible) {
      return 1;
    }
    if (todo1.isDone && !todo2.isDone) {
      return 1;
    }
    if (!todo1.isDone && todo2.isDone) {
      return -1;
    }
    return 0;
  };
  createList(ID_TODO_LIST, todos, compareTodos, createTodoItem);
}

function createMemberList(members, todos, listEventCallback) {
  function createMemberItem(member) {
    // Listen Item erstellen
    const elLi = document.createElement('li');
    elLi.textContent = member.name;
    const todoOfResp = todos.find((todo) => todo.responsible === member._id);
    // Delete Button nur hinzufügen, wenn kein Todo Person
    // zugewiesen ist
    if (!todoOfResp) {
      addDeleteButton(elLi, () =>
        listEventCallback(LIST_EVENT.DELETE, member._id)
      );
    }
    return elLi;
  }

  const compareMembers = (member1, member2) => {
    if (member1 < member2) {
      return -1;
    }
    if (member1 > member2) {
      return 1;
    }
    return 0;
  };
  createList(ID_MEMBER_LIST, members, compareMembers, createMemberItem);
}

function createList(idListEl, items, compareCallback, createItemCallback) {
  // Aufzulistende Items sortieren
  items.sort(compareCallback);
  const elList = document.getElementById(idListEl);
  // Bisherige Liste löschen
  elList.innerHTML = '';
  // Alle aufzulistenden Items in Liste einfügen
  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    const elItem = createItemCallback(item);
    elList.appendChild(elItem);
  }
}

function addDeleteButton(elLi, listEventCallback) {
  const elBtnDel = document.createElement('button');
  elBtnDel.textContent = 'Löschen';
  elBtnDel.addEventListener('click', () => {
    listEventCallback();
  });
  elLi.appendChild(elBtnDel);
}

export { LIST_EVENT, createTodoList, createMemberList };
