import { model as Todo } from './todo.model.js';
import { model as Member } from '../member/member.model.js';

async function getTodos(request, response) {
  await Todo.find()
    .exec()
    .then((todos) => {
      response.json(todos);
    });
}

function checkThatMemberExists(todo) {
  const memberId = todo.responsible;
  return Member.findById(memberId)
    .exec()
    .then((foundMember) => {
      if (!foundMember) {
        return Promise.reject({
          message: `Member with id ${memberId} does not exist.`,
          status: 404,
        });
      }
    });
}

function handleError(error, response) {
  if (error.status) {
    response.status(error.status);
  } else {
    response.status(500);
  }
  response.json({ message: error.message });
}

async function createTodo(request, response) {
  const newTodo = request.body;
  await checkThatMemberExists(newTodo)
    .then(() => {
      return Todo.create({
        description: newTodo.description,
        isDone: newTodo.isDone,
        responsible: newTodo.responsible,
      });
    })
    .then((todo) => {
      response.json(todo);
    })
    .catch((error) => {
      handleError(error, response);
    });
}

function findTodoAndCheckThatItExists(todoId) {
  return Todo.findById(todoId)
    .exec()
    .then((todo) => {
      if (!todo) {
        return Promise.reject({
          message: `TODO with id ${todoId} not found.`,
          status: 404,
        });
      }
      return todo;
    });
}

async function updateTodo(request, response) {
  const todoId = request.params.id;
  const updatedTodo = request.body;
  await checkThatMemberExists(updatedTodo)
    .then(() => {
      return findTodoAndCheckThatItExists(todoId);
    })
    .then((todo) => {
      todo.description = updatedTodo.description;
      todo.isDone = updatedTodo.isDone;
      todo.responsible = updatedTodo.responsible;
      return todo.save();
    })
    .then((savedTodo) => {
      response.json(savedTodo);
    })
    .catch((error) => {
      handleError(error, response);
    });
}

async function removeTodo(request, response) {
  const todoId = request.params.id;
  await findTodoAndCheckThatItExists(todoId)
    .then((todo) => {
      return todo.deleteOne();
    })
    .then((deletedTodo) => {
      response.json(deletedTodo);
    })
    .catch((error) => {
      handleError(error, response);
    });
}

export { getTodos, createTodo, updateTodo, removeTodo };
